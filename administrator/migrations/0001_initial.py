# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Peribahasa',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('huruf', models.CharField(max_length=255, verbose_name='huruf')),
                ('kata', models.CharField(max_length=255, verbose_name='kata')),
                ('arti', models.TextField(verbose_name='arti')),
                ('created', models.DateTimeField(auto_now_add=True, null=True)),
                ('modified', models.DateTimeField(auto_now=True, null=True)),
            ],
            options={
                'db_table': 'peribahasa',
                'verbose_name': 'Daftar Peribahasa',
                'verbose_name_plural': 'Daftar Peribahasa',
            },
        ),
    ]
