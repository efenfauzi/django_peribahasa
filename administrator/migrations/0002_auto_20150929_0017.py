# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('administrator', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='peribahasa',
            name='kata',
            field=models.TextField(max_length=255, verbose_name='kata'),
        ),
    ]
