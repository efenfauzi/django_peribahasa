# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('administrator', '0003_auto_20150929_0029'),
    ]

    operations = [
        migrations.AlterField(
            model_name='peribahasa',
            name='huruf',
            field=models.ForeignKey(related_name='subcategories', blank=True, to='administrator.Peribahasa', null=True),
        ),
    ]
