# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('administrator', '0002_auto_20150929_0017'),
    ]

    operations = [
        migrations.AddField(
            model_name='peribahasa',
            name='peribahasa',
            field=models.TextField(default=datetime.datetime(2015, 9, 29, 0, 29, 38, 667681, tzinfo=utc), max_length=255, verbose_name='peribahasa'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='peribahasa',
            name='kata',
            field=models.CharField(max_length=255, verbose_name='kata'),
        ),
    ]
