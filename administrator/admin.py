# Register your models here.
from django.contrib import admin
from .models import *



class PeribahasaAdmin(admin.ModelAdmin):
    list_display = ('kolom_huruf', 'arti')

admin.site.register(Peribahasa, PeribahasaAdmin)
