# Create your models here.
from django.db import models


class Huruf(models.Model):
    huruf = models.CharField(max_length=2, null=False, blank=False)

class Peribahasa(models.Model):
    #huruf = models.CharField(u'huruf', max_length=255, null=False, blank=False)
    huruf = models.ForeignKey(Huruf,)
    kata = models.CharField(u'kata', max_length=255, null=False, blank=False)
    peribahasa = models.TextField(u'peribahasa', max_length=255, null=False, blank=False)
    arti = models.TextField(u'arti', null=False, blank=False)
    created = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    modified = models.DateTimeField(auto_now=True, null=True, blank=True)

    def __str__(self):
        return self.kata

    class Meta:
        db_table = 'peribahasa'
        verbose_name_plural = u'Daftar Peribahasa'
        verbose_name = u'Daftar Peribahasa'

    def kolom_huruf(self):
        return '<div style="background-color:#FF5100; color:#FFFFFF; text-align:center; font-weight:bold; font-family:Courier New">%s</div>' %self.huruf
    kolom_huruf.allow_tags = True
    kolom_huruf.short_description = u'huruf abjad'